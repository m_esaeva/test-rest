package com.example;


import org.h2.jdbcx.JdbcConnectionPool;
import java.sql.Connection;

public class Test {
    public static void main(String... args) throws Exception {
        JdbcConnectionPool cp = JdbcConnectionPool.create("jdbc:h2:~/test", "admin", "admin");
        Connection conn = cp.getConnection();
        conn.createStatement().execute("Insert into Users (id, LOGIN, PASSWORD, ACTIVE) VALUES (2, 'Chubanov', 124, TRUE );");
        conn.close();
        cp.dispose();
    }
}